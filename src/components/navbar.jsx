import React from 'react';
import {Link} from 'react-router-dom';

const Navbar = () => (
  <ul>
    <li><Link to='/home'>Home</Link></li>
    <li><Link to='/dashboard'>Dashboard</Link></li>
    <li><Link to='/account'>Account</Link></li>
    <li><Link to='/flight/123'>Flight page 123</Link></li>
  </ul>
);

export default Navbar;
