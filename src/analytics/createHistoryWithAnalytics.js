import {createAnalyticsProvider} from './createAnalyticsProvider';
import {reduce} from 'lodash';

export const createHistoryWithAnalytics = (history, providerConfig, pageTransitionConfig) => {
  const analytics = createAnalyticsProvider(providerConfig);

  history.listen((location, action) => {
    const pageEvent = pageTransition(location)(pageTransitionConfig);

    analytics.track(pageEvent);
  });

  return history;
}

const pageTransition = ({pathname}) => (pageTransitionConfig) => {
  const config = pageTransitionConfig[pathname] || pageTransitionConfig.default;

  return reduce(config, (acc, v, k) => {
    return {...acc, [k]: v};
  }, {path: pathname});
}
