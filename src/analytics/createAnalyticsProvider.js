//Create an analytics client using config
export const createAnalyticsProvider = (config) => ({
  track: (data) => console.log('ANALYTICS DATA: ', {...data, ...config})
});
