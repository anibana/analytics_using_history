import React from 'react';
import {connect} from 'react-redux';

const Home = ({name, userId, gender}) => (
  <div>
    <div>
      <label>ID: </label>
      <span>{userId}</span>
    </div>
    <div>
      <label>Name: </label>
      <span>{name}</span>
    </div>
    <div>
      <label>Gender: </label>
      <span>{gender}</span>
    </div>
  </div>
);

const mapStateToProps = ({user}) => ({
  name: user.name,
  userId: user.userId,
  gender: user.gender
});

export default connect(mapStateToProps)(Home);
