import React from 'react';
import {Router, Route} from 'react-router-dom';

import {createHistoryWithAnalytics} from './analytics/createHistoryWithAnalytics';
import {provider, pageTransition} from './analyticsConfig';
import createHistory from 'history/createBrowserHistory';

import Navbar from './components/navbar';
import Account from './pages/Account';
import Home from './pages/Home';
import Dashboard from './pages/Dashboard';
import Flight from './pages/Flight';

const history = createHistory();
const historyWithAnalytics = createHistoryWithAnalytics(history, provider, pageTransition);

const App = () => (
  <Router history={historyWithAnalytics}>
    <div>
      <Navbar />
      <Route path='/home' component={Home} />
      <Route path='/dashboard' component={Dashboard} />
      <Route path='/account' component={Account} />
      <Route path='/flight/:number' component={Flight} />
    </div>
  </Router>
);

export default App;
