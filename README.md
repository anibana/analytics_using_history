# Route listener using modified history object.

## Run in local

- yarn install
- yarn start

**Note:** *This is just an experiment on how the route listener will looks like. For now, Analytics provider will just console.log the events. Later we will replace it with an actual API call.*

**Another Note**: *analytics folder will be extracted to a node module. For now, for demonstration it is part of the React application*

## Sample Configuration

```javascript
export const provider = {
  id: 'PTFUI-123',
  applicationName: 'PTFUI',
  key: 'ABCD'
};

export const pageTransition = {
  '/home': {
    value: 100,
    name: 'home',
  },
  '/dashboard': {
    value: 200,
    name: 'dashboard',
    attr: 'some-random-data'
  },
  '/account': {
    value: 300,
    name: 'account',
    attr: 'another random data'
  },
  '/flight/:number': {
    value: 300,
    name: 'account',
    attr: 'another random data'
  },
  default: {
    value: 10
  }
};

```

### Sample Usage
```javascript
import {provider, pageTransition} from './analyticsConfig';
const history = createHistory();
const historyWithAnalytics = createHistoryWithAnalytics(history, provider, pageTransition);

const App = () => (
  <Router history={historyWithAnalytics}>
    <div>
      <Navbar />
      <Route path='/home' component={Home} />
      <Route path='/dashboard' component={Dashboard} />
      <Route path='/account' component={Account} />
      <Route path='/flight/:number' component={Flight} />
    </div>
  </Router>
);
```

### Sample output
```javascript
{path: "/dashboard", value: 200, name: "dashboard", attr: "some-random-data", applicationName: "PTFUI", id: "PTFUI-123"}
```

### PROS
- single route page tracker config
- easy to install to react app, just need to create a history object and
  plugin it to Router

### CONS
- history object is [mutable](https://reacttraining.com/react-router/web/api/history/history-is-mutable). it's not recommended by React community.
- hard to test
- dependent on react-router library
- Router types like hash and browser router are not supported. [only static router](https://reacttraining.com/react-router/web/api/Router/history-object)
